package com.example.testmap;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;


import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class MainActivity extends FragmentActivity implements OnMarkerClickListener, OnMapLongClickListener, OnMapClickListener {
	static final LatLng TutorialsPoint = new LatLng(21 , 57);
	 boolean markerClicked;
	 PolygonOptions polygonOptions;
	 Polygon polygon;
	 Button buttonchecklocation;
	 private final LocationListener mLocationListener = new LocationListener() {
		    @Override
		    public void onLocationChanged(final Location location) {
		        Log.d("phanbom","location change:"+location.getLongitude()+"|"+location.getLatitude());
		    }

			@Override
			public void onProviderDisabled(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderEnabled(String arg0) {
				// TODO Auto-generated method stub
				  
			}

			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
				// TODO Auto-generated method stub
				
			}
		};
	 private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
		    @Override
		    public void onMyLocationChange(Location location) {
		        LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
		        Marker mMarker = googleMap.addMarker(new MarkerOptions().position(loc));
		        Log.d("phanbom","onchange location");
		        if(googleMap != null){
		        	Log.d("phanbom","toa do :"+loc.latitude + "|"+ loc.longitude);
		        	googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
		        }
		    }
	};
	   private GoogleMap googleMap;
	   protected void onCreate(Bundle savedInstanceState) {
		   Log.d("phanbom","Da vao activity");
	      super.onCreate(savedInstanceState); 
	      setContentView(R.layout.activity_main);
	      googleMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
	   	  googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID); 
	   	  Marker TP = googleMap.addMarker(new MarkerOptions().position(TutorialsPoint).title("TutorialsPoint"));
	   	 
	 /*  	 googleMap.setMyLocationEnabled(true);
	    	CameraUpdate center=
	   	        CameraUpdateFactory.newLatLng(new LatLng(40.76793169992044,
	   	                                                 -73.98180484771729));
	   	    CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);

	   	 googleMap.moveCamera(center);
	   	 googleMap.animateCamera(zoom);
	   	 */
	   	  googleMap.setMyLocationEnabled(true);
	   	  googleMap.setOnMyLocationChangeListener(myLocationChangeListener);
	   	 // Log.d("phanbom", "getMy Location:"+googleMap.getMyLocation().getLatitude()+"|"+googleMap.getMyLocation().getLongitude());
	   	 googleMap.setOnMarkerClickListener(this);
	   	 googleMap.setOnMapLongClickListener(this);
	   	 googleMap.setOnMapClickListener(this);
	   	 buttonchecklocation = (Button)findViewById(R.id.checklocation);
	   	 buttonchecklocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	   	LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

	     mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,5, mLocationListener);
	   }
	@Override
	public boolean onMarkerClick(Marker marker) {
		// TODO Auto-generated method stub
		if(markerClicked){
			if(polygon != null)
			   {
			     polygon.remove();
			     polygon = null;
			   }

			   polygonOptions.add(marker.getPosition());
			   polygonOptions.strokeColor(Color.RED);
			   polygonOptions.fillColor(Color.BLUE);
			   polygon = googleMap.addPolygon(polygonOptions);
			   
	    }
		else
		{
			   if(polygon != null)
			   {
				    polygon.remove();
				    polygon = null;
			   }
			   
				   polygonOptions = new PolygonOptions().add(marker.getPosition());
				   markerClicked = true;
		}
			  
			  return true;
	}
	@Override
	public void onMapLongClick(LatLng point) {
		// TODO Auto-generated method stub
		googleMap.addMarker(new MarkerOptions().position(point).title(point.toString())); 
		markerClicked = false;
	}
	@Override
	public void onMapClick(LatLng arg0) {
		// TODO Auto-generated method stub
		googleMap.animateCamera(CameraUpdateFactory.newLatLng(arg0));  
		markerClicked = false;
	}

	
}
