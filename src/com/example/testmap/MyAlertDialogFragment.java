package com.example.testmap;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class MyAlertDialogFragment extends DialogFragment{
	public static MyAlertDialogFragment newInstance(int title)
	{
		MyAlertDialogFragment frag = new MyAlertDialogFragment();
		Bundle args = new Bundle();
		args.putInt("title", title);
		frag.setArguments(args);
		return frag;
	}
	@Override
	@NonNull
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		int title = getArguments().getInt("title");
		return new AlertDialog.Builder(getActivity())
				
				.create();
	}
  
}
